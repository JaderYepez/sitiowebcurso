/*=========================================================================
								SERVICIOS
=========================================================================*/

$(function(){
	// Animacion en Scrooll
	new WOW().init();
}); 
/*=========================================================================
								WORK 
=========================================================================*/
$(function(){
	$("#work").magnificPopup({
		delegate: 'a',
  		type: 'image',
	  	gallery:{
	    	enabled:true
	  	}
	});
});
/*=========================================================================
								TEAM
=========================================================================*/
$(function(){
	$("#team-members").owlCarousel({
		items:4,
		autoplay:true,
		smartSpeed:700,
		loop: true,
		autoplayHoverPause:true,
		responsive : {
		    // breakpoint from 0 up
		    0 : {
		        items:1
		    },
		    // breakpoint from 480 up
		    480 : {
		        items:2
		    },
		    // breakpoint from 768 up
		    768 : {
		        items:3
		    }   	
		}
	});
});

/*=========================================================================
								TESTIMONIALS
=========================================================================*/
$(function(){
	$("#customers-testimonials").owlCarousel({
		items:1,
		autoplay:true,
		smartSpeed:800,
		loop: true,
		autoplayHoverPause:true
	});
});
/*=========================================================================
								STETS
=========================================================================*/
$(function(){
	 $('.counter').counterUp({
        delay: 10,
        time: 2000
    });
});

/*=========================================================================
								Clients
=========================================================================*/
$(function(){
	$("#clients-list").owlCarousel({
		items:4,
		autoplay:true,
		smartSpeed:700,
		loop: true,
		autoplayHoverPause:true,
		responsive : {
		    // breakpoint from 0 up
		    0 : {
		        items:1
		    },
		    // breakpoint from 480 up
		    480 : {
		        items:3
		    },
		    // breakpoint from 768 up
		    768 : {
		        items:5
		    },
		    // breakpoint from 992 up
		    992:{
		    	items: 6
			}   	
		}
	});
});
/*=========================================================================
								NAVIGATION
=========================================================================*/
//Show // hide transparent black navegation
$(function(){

	$(window).scroll(function(){
		if($(this).scrollTop()<50){
			//desaparece
			$("nav").removeClass("vesco-top-nav");
			$("#back-to-top").fadeOut();
		}else{
			// se muestra el menu
			$("nav").addClass("vesco-top-nav");
			$("#back-to-top").fadeIn();
		}
	});

});
//Smooth scrolling
$(function(){

	$("a.smooth-scroll").click(function(event){
		event.preventDefault();
		//get / return id like #about, #work, #team etc...

		var section=$(this).attr("href");

		$('html,body').animate({
			scrollTop:$(section).offset().top - 64
		},1250, "easeInOutExpo");
	});
});


$(function(){
	$(".navbar-collapse ul li a").on("click touch",function(){
		$(".navbar-toggle").click();
	
	});
});



/*$(function(){

});*/